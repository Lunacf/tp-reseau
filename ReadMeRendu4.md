I. DHCP Client :

🌞 Déterminer : 
```
    PS C:\Users\Utilisateur> ipconfig /all
        Adresse d'un serveur DHCP : 172.16.0.1
        Heure exacte d'obtention du bail DHCP : mercredi 8 novembre 2023 23:45:04
        heure exacte d'expiration du bail DHCP : jeudi 9 novembre 2023 00:09:18
```
        
🌞 Capturer un échange DHCP : 

![🦈DHCP](tp4_dhcp_client.pcapng)


🌞 Analyser la capture Wireshark : 

```
    TRAME OFFERT
```
II. Serveur DHCP :

3. Setup topologie :
        
🌞 Preuve de mise en place :
```
    DHCP:
        [lunacf@localhost ~]$ ping google.com
PING google.com (172.217.18.206) 56(84) bytes of data.
3 packets transmitted, 3 received, 0% packet loss, time 10619ms

    NODE2 : 
        [lunacf@localhost ~]$ ping google.com
PING google.com (172.217.18.206) 56(84) bytes of data.
3 packets transmitted, 3 received, 0% packet loss, time 2002ms


    TRACEROUTE :
         [lunacf@localhost ~]$ traceroute google.com
traceroute to google.com (172.217.18.206), 30 hops max, 60 byte packets
1  _gateway (10.4.1.254)  1.018 ms  1.074 ms  0.806 ms
2  10.0.3.2 (10.0.3.2)  2.011 ms  1.826 ms  1.367 ms
```
4. Serveur DHCP :

🌞 Rendu
```
    [lunacf@localhost ~]$ sudo cat /etc/dhcp/dhcpd.conf
default-lease-time 600;
max-lease-time 7200;
authoritative;
subnet 10.4.1.0 netmask 255.255.255.0 {
    range dynamic-bootp 10.4.1.137 10.4.1.237;
}
```

5. Client DHCP : 

🌞 Test !

node1 : 

[lunacf@localhost ~]$ ip a | grep dynamic
```
inet 10.4.1.137/24 brd 10.4.1.255 scope global dynamic noprefixroute enp0s3
```


[lunacf@localhost ~]$ nmcli con show "System enp0s3" | grep -i dhcp4
```
DHCP4.OPTION[1]:                        dhcp_client_identifier = 01:08:00:27:21:b0:07
DHCP4.OPTION[2]:                        dhcp_lease_time = 600
DHCP4.OPTION[3]:                        dhcp_server_identifier = 10.4.1.253
DHCP4.OPTION[4]:                        expiry = 1699629205
DHCP4.OPTION[5]:                        ip_address = 10.4.1.137
DHCP4.OPTION[6]:                        requested_broadcast_address = 1
DHCP4.OPTION[7]:                        requested_domain_name = 1
DHCP4.OPTION[8]:                        requested_domain_name_servers = 1
DHCP4.OPTION[9]:                        requested_domain_search = 1
DHCP4.OPTION[10]:                       requested_host_name = 1
DHCP4.OPTION[11]:                       requested_interface_mtu = 1
DHCP4.OPTION[12]:                       requested_ms_classless_static_routes = 1
DHCP4.OPTION[13]:                       requested_nis_domain = 1
DHCP4.OPTION[14]:                       requested_nis_servers = 1
DHCP4.OPTION[15]:                       requested_ntp_servers = 1
DHCP4.OPTION[16]:                       requested_rfc3442_classless_static_routes = 1
DHCP4.OPTION[17]:                       requested_root_path = 1
DHCP4.OPTION[18]:                       requested_routers = 1
DHCP4.OPTION[19]:                       requested_static_routes = 1
DHCP4.OPTION[20]:                       requested_subnet_mask = 1
DHCP4.OPTION[21]:                       requested_time_offset = 1
DHCP4.OPTION[22]:                       requested_wpad = 1
DHCP4.OPTION[23]:                       subnet_mask = 255.255.255.0
```

[lunacf@localhost ~]$ ping 10.4.1.254
```
PING 10.4.1.254 (10.4.1.254) 56(84) bytes of data.
64 bytes from 10.4.1.254: icmp_seq=1 ttl=64 time=0.770 ms
64 bytes from 10.4.1.254: icmp_seq=2 ttl=64 time=0.395 ms
64 bytes from 10.4.1.254: icmp_seq=3 ttl=64 time=0.478 ms
64 bytes from 10.4.1.254: icmp_seq=4 ttl=64 time=0.576 ms
^C
--- 10.4.1.254 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3041ms
rtt min/avg/max/mdev = 0.395/0.554/0.770/0.139 ms
```
[lunacf@localhost ~]$ ping 10.4.1.12
```
PING 10.4.1.12 (10.4.1.12) 56(84) bytes of data.
64 bytes from 10.4.1.12: icmp_seq=1 ttl=64 time=0.667 ms
64 bytes from 10.4.1.12: icmp_seq=2 ttl=64 time=1.13 ms
64 bytes from 10.4.1.12: icmp_seq=3 ttl=64 time=1.75 ms
64 bytes from 10.4.1.12: icmp_seq=4 ttl=64 time=1.27 ms
^C
--- 10.4.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3068ms
rtt min/avg/max/mdev = 0.667/1.203/1.752/0.386 ms
```

🌞 Bail DHCP serveur

[lunacf@localhost ~]$ cat /var/lib/dhcpd/dhcpd.leases
```
authoring-byte-order little-endian;

lease 10.4.1.137 {
  starts 5 2023/11/10 15:28:24;
  ends 5 2023/11/10 15:38:24;
  cltt 5 2023/11/10 15:28:24;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 08:00:27:21:b0:07;
  uid "\001\010\000'!\260\007";
}
lease 10.4.1.137 {
  starts 5 2023/11/10 15:33:24;
  ends 5 2023/11/10 15:43:24;
  cltt 5 2023/11/10 15:33:24;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 08:00:27:21:b0:07;
  uid "\001\010\000'!\260\007";
}
 
```
6. Options DHCP

[lunacf@localhost network-scripts]$ cat ifcfg-enp0s3
```
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.4.1.253
NETMASK=255.255.255.0

option routers 10.4.1.254;
option domain-name-servers 8.8.8.8;
default-lease-time 600;
max-lease-time 21600;

```

🌞 Test !

[lunacf@localhost ~]$ sudo nmcli con reload

[lunacf@localhost ~]$ sudo nmcli con up "System enp0s3"
```
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/3)
```
[lunacf@localhost ~]$ sudo systemctl restart NetworkManager

[lunacf@localhost ~]$ cat /etc/resolv.conf
```
; generated by /usr/sbin/dhclient-script
nameserver 8.8.8.8
```

[lunacf@localhost ~]$ ping 8.8.8.8
```
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=18.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=21.7 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=20.8 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=112 time=21.1 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 18.495/20.531/21.740/1.222 ms
```

🌞 Capture Wireshark
```
