Rendu TP

🌞interface Wifi : 
    PS C:\WINDOWS\system32> ipconfig /all
        Realtek RTL8821CE 802.11ac PCIe Adapter
        B0-68-E6-BC-EF-E3
        10.33.48.42
        
🌞interface Ethernet :
    je n'ai pas de port ethernet donc je n'ai pas d'interface ethernet

🌞adresse ip de la passerelle :
    PS C:\WINDOWS\system32> ipconfig /all
        10.33.51.254

🌞adresse mac de la passerelle :
    arp /a 10.33.51.254
        7c-5a-1c-cb-fd-a4

🌞 informations sur une carte IP : 
        interface Wifi : 
            IP : 10.33.48.42
            MAC : B0-68-E6-BC-EF-E3
            gateway : 10.33.51.254

🌞 Changer d'adresse IP : 
        la nouvelle adresse est 10.33.48.40

🌞Perte d'internet :
    il s'agit de l'IP d'un autre utilisateur ainsi,
    puisqu'il était présent avant nous, c'est à lui
    que sont délivrées les informations



II- 
🌞partie à deux impossible sans port Ethernet
    suivi sur l'ordinateur de mes camarades Brendan et Sonita.

    modification de l'IP des deux machines
        -> changement d'adresse IP : 10.10.10.24
        -> changement de masque : 255.255.255.0

🌞vérification du changement d'IP :
    ipv4 : 10.10.10.24
    masque de sous-réseau : 255.255.255.0
    auto-config : 169.254.130.203
    masque de sous-réseau : 255.255.0.0

🌞les deux machines se joignent :
    $ ping 10.10.10.24
    Envoi d’une requête 'Ping'  10.10.10.24 avec 32 octets de données :
    Réponse de 10.10.10.24 : octets=32 temps<1ms TTL=128
    Réponse de 10.10.10.24 : octets=32 temps<1ms TTL=128
    Réponse de 10.10.10.24 : octets=32 temps<1ms TTL=128
    Réponse de 10.10.10.24 : octets=32 temps<1ms TTL=128

🌞adresse MAC du correspondant :
    arp -a 
        2c-f0-5d-66-be-f2


🌞 connexion entre deux machines par carte réseau wifi :
    PS C:\Users\Utilisateur\Downloads\netcat-win32-1.11\netcat-1.11> .\nc.exe 10.33.48.37 8833
    dfvgbn,;
    sdfyxgd


🌞Exploration du DHCP
    PS C:\WINDOWS\system32> ipconfig /all
        adresse ip du serveur DHCP du réseau wifi ynov : 
            10.33.51.254
        date d'expiration de votre bail DHCP : 
            mardi 17 octobre 2023 13:27:28


🌞 DNS
    PS C:\WINDOWS\system32> ipconfig /all
        adresse IP du serveur DNS : 
            10.33.10.2