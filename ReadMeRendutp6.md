II. Serveur DNS
    2. Setup

🌞 Dans le rendu                              :

[lunacf@dns ~]$ sudo cat /var/named/tp6.b1.rev

```
$TTL 86400
@ IN SOA dns.tp6.b1. admin.tp6.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.tp6.b1.

; Reverse lookup
101 IN PTR dns.tp6.b1.
11 IN PTR john.tp6.b1.
```
[lunacf@dns ~]$ sudo cat /var/named/tp6.b1.db

```
$TTL 86400
@ IN SOA dns.tp6.b1. admin.tp6.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.tp6.b1.

; Enregistrements DNS pour faire correspondre des noms à des IPs
dns       IN A 10.6.1.101
john      IN A 10.6.1.11
```

[lunacf@dns ~]$ sudo cat /etc/named.conf

```
options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { localhost; any; };
        allow-query-cache {localhost; any; };

        recursion yes;

        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";
        geoip-directory "/usr/share/GeoIP";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "tp6.b1" IN {
        type master;
        file "tp6.b1.db";
        allow-update { none; };
        allow-query {any; };
};

zone "1.4.10.10.in-addr.arpa" IN {
        type master;
        file "tp6.b1.rev";
        allow-update { none; };
        allow-query {any; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

[lunacf@dns ~]$ sudo systemctl status named

```
● named.service - Berkeley Internet Name Domain (DNS)
     Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; preset: disabled)
     Active: active (running) since Fri 2023-11-17 16:28:51 CET; 33min ago
   Main PID: 1964 (named)
      Tasks: 5 (limit: 4673)
     Memory: 20.2M
        CPU: 87ms
     CGroup: /system.slice/named.service
             └─1964 /usr/sbin/named -u named -c /etc/named.conf

Nov 17 16:28:51 dns named[1964]: network unreachable resolving './NS/IN': 2001:500:a8::e#53
Nov 17 16:28:51 dns named[1964]: zone tp6.b1/IN: loaded serial 2019061800
Nov 17 16:28:51 dns named[1964]: zone 1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa/IN: loaded serial 0
Nov 17 16:28:51 dns named[1964]: zone localhost/IN: loaded serial 0
Nov 17 16:28:51 dns named[1964]: zone localhost.localdomain/IN: loaded serial 0
Nov 17 16:28:51 dns named[1964]: all zones loaded
Nov 17 16:28:51 dns systemd[1]: Started Berkeley Internet Name Domain (DNS).
Nov 17 16:28:51 dns named[1964]: running
Nov 17 16:28:51 dns named[1964]: managed-keys-zone: Initializing automatic trust anchor management for zone '.'; DNSKEY ID 20326 is now trusted, wa>
Nov 17 16:28:51 dns named[1964]: resolver priming query complete
```
[lunacf@dns ~]$ sudo ss -l -n

```
udp   UNCONN 0      0                                   10.6.1.101:53                               0.0.0.0:*
udp   UNCONN 0      0                                    127.0.0.1:53                               0.0.0.0:*
udp   UNCONN 0      0                                        [::1]:323                                 [::]:*
udp   UNCONN 0      0                                        [::1]:53                                  [::]:*
tcp   LISTEN 0      128                                    0.0.0.0:22                               0.0.0.0:*
tcp   LISTEN 0      4096                                 127.0.0.1:953                              0.0.0.0:*
tcp   LISTEN 0      10                                  10.6.1.101:53                               0.0.0.0:*
tcp   LISTEN 0      10                                   127.0.0.1:53                               0.0.0.0:*
tcp   LISTEN 0      4096                                     [::1]:953                                 [::]:*
tcp   LISTEN 0      128                                       [::]:22                                  [::]:*
tcp   LISTEN 0      10                                       [::1]:53                                  [::]:*

```

🌞 Ouvrez le bon port dans le firewall

[lunacf@dns ~]$ sudo firewall-cmd --add-port=53/udp --permanent
```
success
```
[lunacf@dns ~]$ sudo firewall-cmd --reload
```
success
```
[lunacf@dns ~]$ sudo firewall-cmd --list-all
```
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 53/udp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

3. Test

🌞 Sur la machine john.tp6.b1

[lunacf@john etc]$ sudo cat resolv.conf
```
DNS=10.6.1.101
```

[lunacf@john etc]$ dig john.tp6.b1
```
; <<>> DiG 9.16.23-RH <<>> john.tp6.b1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 7500
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 5dbb28652391944f01000000655c9d7513eda767c8985e2b (good)
;; QUESTION SECTION:
;john.tp6.b1.                   IN      A

;; ANSWER SECTION:
john.tp6.b1.            86400   IN      A       10.6.1.11

;; Query time: 1 msec
;; SERVER: 10.6.1.101#53(10.6.1.101)
;; WHEN: Tue Nov 21 13:02:28 CET 2023
;; MSG SIZE  rcvd: 84
```

[lunacf@john ~]$ dig dns.tp6.b1
```
; <<>> DiG 9.16.23-RH <<>> dns.tp6.b1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 17302
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: d99d817eed4db60e01000000655c9d36f9213c288c7ad261 (good)
;; QUESTION SECTION:
;dns.tp6.b1.                    IN      A

;; ANSWER SECTION:
dns.tp6.b1.             86400   IN      A       10.6.1.101

;; Query time: 1 msec
;; SERVER: 10.6.1.101#53(10.6.1.101)
;; WHEN: Tue Nov 21 13:06:15 CET 2023
;; MSG SIZE  rcvd: 83
```

[lunacf@dhcp network-scripts]$ dig john.tp6.b1 @10.6.1.101
```
; <<>> DiG 9.16.23-RH <<>> john.tp6.b1 @10.6.1.101
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 61001
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: c7040d3cd7eec96001000000655f7109f79c58860bafee42 (good)
;; QUESTION SECTION:
;john.tp6.b1.                   IN      A

;; ANSWER SECTION:
john.tp6.b1.            86400   IN      A       10.6.1.11

;; Query time: 0 msec
;; SERVER: 10.6.1.101#53(10.6.1.101)
;; WHEN: Thu Nov 23 16:34:33 CET 2023
;; MSG SIZE  rcvd: 84
```


🌞 Sur votre PC

```
	10.6.1.101 	nameserver	#DNS.tp6.b1
	10.6.1.11 	john		#john.tp6.b1

	recursion yes;
```
```
PS C:\Users\Utilisateur> nslookup john.tp6.b1 10.6.1.101
Serveur :   UnKnown
Address:  10.6.1.101

Nom :    john.tp6.b1
Address:  10.6.1.11
```

scp lunacf@10.6.1.11:/lunacf/tp6_dns.pcapng ./
![tp6_dns.pcapng🦈](tp6_dns.pcapng)

III. Serveur DHCP

🌞 Installer un serveur DHCP

[lunacf@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
```
option domain-name-servers     10.6.1.101;
default-lease-time 600;
max-lease-time 7200;
authoritative;

subnet 10.6.1.0 netmask 255.255.255.0 {
        range dynamic-bootp 10.6.1.13 10.6.1.37;
        option routers 10.6.1.254;
}
```

🌞 Test avec john.tp6.b1

[lunacf@john ~]$ ip a
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:64:e7:0d brd ff:ff:ff:ff:ff:ff
    inet 10.6.1.13/24 brd 10.6.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 403sec preferred_lft 403sec
    inet6 fe80::a00:27ff:fe64:e70d/64 scope link
       valid_lft forever preferred_lft forever
```

[lunacf@john ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
```
DEVICE=enp0s3

BOOTPROTO=dhcp
ONBOOT=yes

NETMASK=255.255.255.0
```

[lunacf@john ~]$ ip r s
```
default via 10.6.1.254 dev enp0s3 proto dhcp src 10.6.1.13 metric 100
10.6.1.0/24 dev enp0s3 proto kernel scope link src 10.6.1.13 metric 100
```

[lunacf@john ~]$ ping 8.8.8.8
```
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=487 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=22.0 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=23.2 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 21.983/177.296/486.668/218.759 ms
```

[lunacf@john ~]$ sudo cat /etc/resolv.conf
```
# Generated by NetworkManager
nameserver 10.6.1.101
```

🌞 Requête web avec john.tp6.b1

[lunacf@john ~]$ curl www.ynov.com
```
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>302 Found</title>
</head><body>
<h1>Found</h1>
<p>The document has moved <a href="https://www.ynov.com/">here</a>.</p>
<hr>
<address>Apache/2.4.41 (Ubuntu) Server at ynov.com Port 80</address>
</body></html>
```


![🦈 Capture tp6_web](tp6_web.pcap)




