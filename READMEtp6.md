# TP6 : Un LAN maîtrisé meo ?

Dans ce TP on va mettre en oeuvre quelques services réseau, pour approfondir votre culture sur les services les plus répandus (comme DNS) et pour mettre en oeuvre le savoir sur TCP et UDP.

En vrai, après ça, vous aurez eu un bon aperçu de ce qui se fait dans la plupart des LANs du monde :

- **un routeur qui permet d'accéder à internet**
  - il agira donc comme la passerelle du réseau LAN
- **un switch/accès WiFi**
  - pour que tout le monde se connecte au routeur
  - c'est nos p'tits réseaux privé-hôte dans VirtualBox
- **un serveur DNS**
  - pour que les clients puissent résoudre des noms depuis le LAN
- **un serveur DHCP**
  - il file des IPs aux clients
  - et les informe de l'adresse IP de la passerelle du LAN
  - et aussi de l'adresse IP du serveur DNS du LAN
- **l'utilisation de TCP et UDP**
  - pour établir des connexions et faire des trucs utiles
  - une fois que le setup IP est terminé :
    - client a une IP
    - il connaît l'adresse de la passerelle
    - il connaît l'adresse d'un serveur DNS

---

![Not the network](./img/not_the_network.jpg)

## Sommaire

- [TP6 : Un LAN maîtrisé meo ?](#tp6--un-lan-maîtrisé-meo-)
  - [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
- [I. Setup routeur](#i-setup-routeur)
- [II. Serveur DNS](#ii-serveur-dns)
  - [1. Présentation](#1-présentation)
  - [2. Setup](#2-setup)
  - [3. Test](#3-test)
- [III. Serveur DHCP](#iii-serveur-dhcp)
- [IV. Bonus : Serveur Web HTTPS](#iv-bonus--serveur-web-https)

# 0. Prérequis

➜ **Pour ce TP, on va se servir de VMs Rocky Linux** quand des VMs seront nécessaires. On va en faire plusieurs, n'hésitez pas à diminuer la RAM (512Mo ou 1Go devraient suffire). Vous pouvez redescendre la mémoire vidéo aussi.  

➜ **Si vous voyez un 🦈** c'est qu'il y a une capture PCAP à produire et à mettre dans votre dépôt git de rendu

➜ **L'emoji 🖥️ indique une VM à créer**. Pour chaque VM, vous déroulerez la checklist suivante :

- [x] Créer la machine (avec une carte host-only)
- [x] Définir une IP statique à la VM
- [x] Donner un hostname à la machine
- [x] Utiliser SSH pour administrer la machine
- [x] Remplir votre fichier `hosts`, celui de votre PC, pour accéder au VM avec un nom
- [x] Dès que le routeur est en place, n'oubliez pas d'ajouter une route par défaut aux autres VM pour qu'elles aient internet
- [x] Dès que le serveur DNS est en place, n'oubliez pas de l'ajouter à la conf des autres machines à la main si nécessaire

> Toutes les commandes pour réaliser ces opérations sont dans [le mémo Rocky](../../cours/memo/rocky_network.md). Aucune de ces étapes ne doit figurer dan le rendu, c'est juste la mise en place de votre environnement de travail.

# I. Setup routeur

![Topo 1](./img/topo1.svg)

| Name            | LAN1 `10.6.1.0/24` |
| --------------- | ------------------ |
| `router.tp6.b1` | `10.6.1.254`       |
| `john.tp6.b1`   | `10.6.1.11`        |

🖥️ **Machine `router.tp6.b1`**

- ajoutez lui aussi une carte NAT en plus de la carte host-only (privé hôte en français) pour qu'il ait un accès internet
- toutes les autres VMs du TP devront utiliser ce routeur pour accéder à internet
- n'oubliez pas d'activer le routage vers internet sur cette machine :

```
$ sudo firewall-cmd --add-masquerade --permanent
$ sudo firewall-cmd --reload
```

🖥️ **Machine `john.tp6.b1`**

- une machine qui servira de client au sein du réseau pour effectuer des tests
- suivez-bien la checklist !
- testez tout de suite avec `john` que votre routeur fonctionne et que vous avez un accès internet

# II. Serveur DNS

![Haiku DNS](./img/haiku_dns.png)

## 1. Présentation

Un serveur DNS est un serveur qui est capable de répondre à des requêtes DNS.

Une requête DNS est la requête effectuée par une machine lorsqu'elle souhaite connaître l'adresse IP d'une machine, lorsqu'elle connaît son nom.

Par exemple, si vous ouvrez un navigateur web et saisissez `https://www.ynov.com` alors une requête DNS est automatiquement effectuée par votre PC pour déterminez à quelle adresse IP correspond le nom `www.ynov.com`.

> *La partie `https://` ne fait pas partie du nom de domaine, ça indique simplement au navigateur la méthode de connexion. Ici, c'est HTTPS.*

Dans cette partie, on va monter une VM qui porte un serveur DNS. Ce dernier répondra aux autres VMs du LAN quand elles auront besoin de connaître des noms. Ainsi, ce serveur pourra :

- résoudre des noms locaux
  - vous pourrez `ping john.tp6.b1` et ça fonctionnera
  - mais aussi `ping www.ynov.com` et votre serveur DNS sera capable de le résoudre aussi

*Dans la vraie vie, il n'est pas rare qu'une entreprise gère elle-même ses noms de domaine, voire gère elle-même son serveur DNS. C'est donc du savoir très proche de ce qu'on fait dans la vraie vie dont il est question.*

> En réalité, ce n'est pas votre serveur DNS qui pourra résoudre `www.ynov.com`, mais il sera capable de *forward* (faire passer) votre requête à un autre serveur DNS qui lui, connaît la réponse.

## 2. Setup

![Topo 2](./img/topo2.svg)

| Name            | LAN1 `10.6.1.0/24` |
| --------------- | ------------------ |
| `router.tp6.b1` | `10.6.1.254`       |
| `john.tp6.b1`   | `10.6.1.11`        |
| `dns.tp6.b1`    | `10.6.1.101`       |

🖥️ **Machine `dns.tp6.b1`**

- n'oubliez pas de dérouler la checklist

> *Sur internet ça peut partir dans tous les sens, alors je vous ai écrit un petit tuto maison cette fois hehe. **Lisez attentivement toute cette partie en entier avant de commencer les manips.** Pour mieux capter ce que vous faites.*

Installation du serveur DNS :

```bash
# installation du serveur DNS, son p'tit nom c'est BIND9
$ sudo dnf install -y bind bind-utils
```

La configuration du serveur DNS va se faire dans 3 fichiers essentiellement :

- **un fichier de configuration principal**
  - `/etc/named.conf`
  - on définit les trucs généraux, comme les adresses IP et le port où on veu écouter
  - on définit aussi un chemin vers les autres fichiers, les fichiers de zone
- **un fichier de zone**
  - `/var/named/tp6.b1.db`
  - je vous préviens, la syntaxe fait mal
  - on peut y définir des correspondances `nom ---> IP`
- **un fichier de zone inverse**
  - `/var/named/tp6.b1.rev`
  - on peut y définir des correspondances `IP ---> nom`

➜ **Allooooons-y, fichier de conf principal**

- je ne vous mets que les lignes les plus importantes, et celles qu'on modifie
- les `[...]` indiquent qu'il faut laisser la conf par défaut, que j'ai enlevé ici pour que ce soit + lisible
  - il ne faut donc pas les mettre dans vos fichiers

```bash
# éditez le fichier de config principal pour qu'il ressemble à :
$ sudo cat /etc/named.conf
options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
[...]
        allow-query     { localhost; any; };
        allow-query-cache { localhost; any; };

        recursion yes;
[...]
# référence vers notre fichier de zone
zone "tp6.b1" IN {
     type master;
     file "tp6.b1.db";
     allow-update { none; };
     allow-query {any; };
};
# référence vers notre fichier de zone inverse
zone "1.4.10.in-addr.arpa" IN {
     type master;
     file "tp6.b1.rev";
     allow-update { none; };
     allow-query { any; };
};
```

➜ **Et pour les fichiers de zone**

- dans ces fichiers c'est le caractère `;` pour les commentaires
- hésitez pas à virer mes commentaires de façon générale
- c'juste pour que vous captiez mais ça fait dégueu dans un fichier de conf

```bash
# Fichier de zone pour nom -> IP

$ sudo cat /var/named/tp6.b1.db

$TTL 86400
@ IN SOA dns.tp6.b1. admin.tp6.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.tp6.b1.

; Enregistrements DNS pour faire correspondre des noms à des IPs
dns       IN A 10.6.1.101
john      IN A 10.6.1.11
```

```bash
# Fichier de zone inverse pour IP -> nom

$ sudo cat /var/named/tp6.b1.rev

$TTL 86400
@ IN SOA dns.tp6.b1. admin.tp6.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.tp6.b1.

; Reverse lookup
101 IN PTR dns.tp6.b1.
11 IN PTR john.tp6.b1.
```

➜ **Une fois ces 3 fichiers en place, démarrez le service DNS**

```bash
# Démarrez le service tout de suite
$ sudo systemctl start named

# Faire en sorte que le service démarre tout seul quand la VM s'allume
$ sudo systemctl enable named

# Obtenir des infos sur le service
$ sudo systemctl status named

# Obtenir des logs en cas de probème
$ sudo journalctl -xe -u named
```

🌞 **Dans le rendu, je veux**

- un `cat` des 3 fichiers de conf (fichier principal + deux fichiers de zone)
- un `systemctl status named` qui prouve que le service tourne bien
- une commande `ss` qui prouve que le service écoute bien sur un port

🌞 **Ouvrez le bon port dans le firewall**

- grâce à la commande `ss` vous devrez avoir repéré sur quel port tourne le service
  - vous l'avez écrit dans la conf aussi toute façon :)
- ouvrez ce port dans le firewall de la machine `dns.tp6.b1` (voir le mémo réseau Rocky)

## 3. Test

🌞 **Sur la machine `john.tp6.b1`**

- configurez la machine pour qu'elle utilise votre serveur DNS quand elle a besoin de résoudre des noms
  - voir mémo pour ça aussi
  - **`john` ne doit connaître qu'un seul serveur DNS : le vôtre**
  - une seule IP renseignée dans le fichier `resolv.conf`
- assurez-vous que vous pouvez :
  - résoudre des noms comme `john.tp6.b1` et `dns.tp6.b1`
  - mais aussi des noms comme `www.ynov.com`

> On dit donc que votre serveur DNS est autoritatif pour la zone `tp6.b1` et qu'il est aussi capable de forward des requêtes pour des noms publics vers un autre serveur DNS.

🌞 **Sur votre PC**

- utilisez une commande pour résoudre le nom `john.tp6.b1` en utilisant `10.6.1.101` comme serveur DNS

> Le fait que votre serveur DNS puisse résoudre un nom comme `www.ynov.com`, ça s'appelle la récursivité et c'est activé avec la ligne `recursion yes;` dans le fichier de conf.

🦈 **Capture `tp6_dns.pcapng`**

- une requête DNS vers le nom `john.tp6.b1` ainsi que la réponse
- prenez deux minutes pour regarder le contenu de la trame
- les requêtes DNS passent en clair, rien n'est chiffré : on voit tout !
- capture à réaliser avec une commande `tcpdump` sur l'un des VMs

# III. Serveur DHCP

> *On prend les mêmes et on r'commence.*

![Topo 3](./img/topo3.svg)

| Name            | LAN1 `10.6.1.0/24` |
| --------------- | ------------------ |
| `router.tp6.b1` | `10.6.1.254`       |
| `john.tp6.b1`   | `DHCP`             |
| `dns.tp6.b1`    | `10.6.1.101`       |
| `dhcp.tp6.b1`   | `10.6.1.102`       |

🖥️ **Machine `dhcp.tp6.b1`**

- déroulez la checklisteuh
- indiquez votre propre serveur DNS pour la résolution de noms

🌞 **Installer un serveur DHCP**

- référez-vous à vos TPs précédents
- il doit attribuer des adresses IP entre `10.6.1.13` et `10.6.1.37`
- il doit informer les clients de l'adresse de la passerelle du réseau : `10.6.1.254`
- il doit informer les clients de l'adresse du serveur DNS du réseau : `10.6.1.101`

🌞 **Test avec `john.tp6.b1`**

- enlevez-lui son IP statique, et récupérez une IP en DHCP
- prouvez-que
  - vous avez une IP dans la bonne range
  - vous avez votre routeur configuré automatiquement comme passerelle
  - vous avez votre serveur DNS automatiquement configuré
  - vous avez un accès internet

🌞 **Requête web avec `john.tp6.b1`**

- utilisez la commande `curl` pour faire une requête HTTP vers le site de votre choix
- par exemple `curl www.ynov.com`

🦈 **Capture `tp6_web.pcapng`**

- capture à réaliser avec une commande `tcpdump` sur l'une des machines
- je veux voir dans la capture :
  - la requête DNS qui est effectuée automatiquement
  - la réponse DNS
  - le 3-way handshake TCP vers le serveur web
  - l'échange HTTP/HTTPS

> *Hé. Tu le sens qu'on se rapproche vraiment d'une situation réelle là ?*

# IV. Bonus : Serveur Web HTTPS

Cette partie est un **bonus** : juste pour les téméraires donc.

L'objectif est simple : monter un serveur web comme dans le TP précédent, mais le servir avec HTTPS et pas juste HTTP, afin de fournir une connexion de confiance aux visiteurs du site.

| Name            | LAN1 `10.6.1.0/24` |
| --------------- | ------------------ |
| `router.tp6.b1` | `10.6.1.254`       |
| `john.tp6.b1`   | `DHCP`             |
| `dns.tp6.b1`    | `10.6.1.101`       |
| `dhcp.tp6.b1`   | `10.6.1.102`       |
| `web.tp6.b1`    | `10.6.1.103`       |

Pour passer de HTTP à HTTPS avec un serveur NGINX ça peut se résumer à :

- taper une commande pour générer une clé et un certificat
- ajouter 3 lignes de conf dans la conf existante

C'est si peu que je vous laisse google "simple nginx https configuration" par exemple ! Hésitez-pas à me demander si besoin d'aide, mais c'est un exercice classique et formateur :)

➜ **On doit donc pouvoir visiter votre site web localement, depuis les VMs, en tapant `https://web.tp6.b1`.**
