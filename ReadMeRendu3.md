🌞  Générer des requêtes ARP

⭐️   Marcel

[lunacf@localhost ~]$ ping 10.3.1.11 //PING
```
    --- 10.3.1.11 ping statistics ---
    3 packets transmitted, 3 received, 0% packet loss, time 2055ms
    rtt min/avg/max/mdev = 0.406/1.037/1.417/0.449 ms
```

[lunacf@localhost ~]$ ip n s //TABLE ARP
```
    10.3.1.11 dev enp0s3 lladdr 08:00:27:5a:45:15 STALE
    10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:39 DELAY
```

[lunacf@localhost ~]$ ip a // IP
```
    link/ether 08:00:27:4e:8e:97 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.12/24 brd 10.3.1.255 scope global noprefixroute enp0s3
```

   ⭐️  John

ping 10.3.1.12 //PING
```
    --- 10.3.1.12 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3050ms
    rtt min/avg/max/mdev = 0.486/1.199/1.540/0.417 ms
```

[lunacf@localhost ~]$ ip n s //TABLE ARP
```
    10.3.1.12 dev enp0s3 lladdr 08:00:27:4e:8e:97 STALE
    10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:39 DELAY
```

[lunacf@localhost ~]$ ip a // IP
```
    link/ether 08:00:27:5a:45:15 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.11/24 brd 10.3.1.255 scope global noprefixroute enp0s3
```


🌞  Analyse de trames


![capture🦈](tp3.arp.pcap)


🌞Ajouter les routes statiques nécessaires pour que john et marcel puissent se ping


⭐️ MARCEL :  
[lunacf@localhost ~]$ sudo ip route add 10.3.1.11 via 10.3.2.254 dev enp0s3 

[lunacf@localhost ~]$ ping 10.3.1.11
```
    PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
```


⭐️ JOHN :
[lunacf@localhost ~]$ sudo ip route add 10.3.2.12 via 10.3.1.254 dev enp0s3

[lunacf@localhost ~]$ ping 10.3.2.12
```
        PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
```

🌞Analyse des échanges ARP

⭐️ JOHN : 

[lunacf@localhost ~]$ ip n s
```
        10.3.1.254 dev enp0s3 lladdr 08:00:27:52:84:3e STALE
        10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:39 REACHABLE
```

⭐️ MARCEL : 

[lunacf@localhost ~]$ ip n s
```
        10.3.2.254 dev enp0s3 lladdr 08:00:27:9a:08:5b STALE
        10.3.2.1 dev enp0s3 lladdr 0a:00:27:00:00:3e REACHABLE
```


| ordre | type trame  | IP source |         MAC source         | IP destination |       MAC destination      |
| ----- | ----------- | --------- | -------------------------- | -------------- | -------------------------- |
| 1     | Requête ARP | X         | `marcel``08:00:27:5a:45:15`| X              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | X         | `john``08:00:27:52:84:3e`  | X              | `marcel``08:00:27:5a:45:15`|
| ...   | ...         | ...       | ...                        |                |                            |
| ?     | Ping        | 10.3.2.12 |   08:00:27:52:84:3e        | 10.3.1.11      |     08:00:27:5a:45:15      |
| ?     | Pong        | 10.3.1.11 |   08:00:27:5a:45:15        | 10.3.2.12      |     08:00:27:52:84:3e      |



🌞Donnez un accès internet à vos machines : 

[lunacf@localhost ~]$ sudo firewall-cmd --add-masquerade --permanent

[lunacf@localhost ~]$ sudo firewall-cmd --reload

[lunacf@localhost ~]$ ping 8.8.8.8
```
    --- 8.8.8.8 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3006ms
```

🌞Donnez un accès internet à vos machines : 

⭐️ JOHN :
[lunacf@localhost ~]$ sudo ip route add default via 10.3.1.254 dev enp0s3

[lunacf@localhost ~]$ ping 8.8.8.8
```
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    10 packets transmitted, 10 received, 0% packet loss, time 9017ms
```

[lunacf@localhost ~]$ ping gitlab.com
```
    --- gitlab.com ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 15174ms
```

⭐️ MARCEL :
[lunacf@localhost ~]$ sudo ip route add default via 10.3.1.254 dev enp0s3

[lunacf@localhost ~]$ ping 8.8.8.8
```
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    10 packets transmitted, 10 received, 0% packet loss, time 9023ms
```

[lunacf@localhost ~]$ ping gitlab.com
```
    --- gitlab.com ping statistics ---
    6 packets transmitted, 6 received, 0% packet loss, time 25252ms
```


🌞Analyse de trames: 

![🦈capture réseau lan1](tp3_routage_lan1.pcap)

![🦈capture réseau lan2][tp3_routage_lan2.pcap]



|ordre|type trame| IP source          | MAC source                 | IP destination      | MAC destination           |
|-----|----------|--------------------|----------------------------|---------------------|---------------------------|
|1    | ping     |`john 10.3.1.11`    |`john 08:00:27:52:84:3e`    |`routeur 10.3.1.254` |`routeur 08:00:27:21:87:11`|
|4    | pong     |`routeur 10.3.1.254`|`routeur 08:00:27:21:87:11` |`john 10.3.1.11`     |`john 08:00:27:52:84:3e`   |

|ordre|type trame| IP source          | MAC source                 | IP destination      | MAC destination           |
|-----|----------|--------------------|----------------------------|---------------------|---------------------------|
|2    | ping     |`routeur 10.3.2.254`|`routeur 08:00:27:21:87:11` |`marcel 10.3.2.12`   |`marcel 08:00:27:5a:45:15` |
|3    | pong     |`marcel 10.3.2.12`  |`marcel 08:00:27:21:87:11`  |`routeur 10.3.2.254` |`routeur 08:00:27:21:87:11`|