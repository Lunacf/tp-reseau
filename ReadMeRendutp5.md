I. First steps

🌞 Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP

![ynov :](tp5_service_1.pcapng) 
```
TCP
port dest = 443
ip dest = 142.250.75.234
port source = 56056
```

![Pinterest :](tp5_service_2.pcapng)
```
TCP
port dest = 443
ip dest = 2.20.88.229
port source = 56195
```
![Reverso :](tp5_service_3.pcapng)
```
TCP
port dest = 443
ip dest = 142.250.178.131
port source = 56248
```
![Youtube :](tp5_service_4.pcapng)
```
UDP
port dest = 443
ip dest = 172.217.20.164
port source = 56135
```
![Shein :](tp5_service_5.pcapng)
```
TCP
port dest = 443
ip dest = 96.17.193.19
port source = 56421
```


🌞 Demandez l'avis à votre OS

PS C:\Users\Utilisateur> netstat -a -n -b | Select-String chrome -Context 1,0 : 

YNOV:
```
>  [chrome.exe]
    TCP    10.33.70.184:57479     142.250.75.234:443     ESTABLISHED
```

PINTEREST:
```
>  [chrome.exe]
    TCP    10.33.70.184:57552     2.20.88.229:443        ESTABLISHED
>  [chrome.exe]
    TCP    10.33.70.184:57553     2.20.88.229:443        ESTABLISHED
>  [chrome.exe]
    TCP    10.33.70.184:57554     2.20.88.229:443        ESTABLISHED
```

YOUTUBE:
```
>  [chrome.exe]
    TCP    10.33.70.184:57717     172.217.20.174:443     ESTABLISHED
>  [chrome.exe]
    TCP    10.33.70.184:57718     172.217.20.164:443     ESTABLISHED
```


II. Setup Virtuel

🌞 Examinez le trafic dans Wireshark


🌞 Demandez aux OS



![🦈3 way handshake]()

2. Routage


🌞 Prouvez que

node1.tp5.b1 a un accès internet :

[lunacf@node ~]$ ping 8.8.8.8
```
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=238 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=20.1 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 20.127/128.852/237.577/108.725 ms
```


node1.tp5.b1 peut résoudre des noms de domaine publics 

[lunacf@node ~]$ ping www.ynov.com
```
PING www.ynov.com (172.67.74.226) 56(84) bytes of data.
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=1 ttl=52 time=345 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=2 ttl=52 time=276 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=3 ttl=52 time=20.5 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=4 ttl=52 time=21.0 ms
64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=5 ttl=52 time=21.7 ms
^C
--- www.ynov.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4006ms
rtt min/avg/max/mdev = 20.470/136.891/345.290/143.574 ms
```

3. Serveur Web

🌞 Installez le paquet nginx

[lunacf@web ~]$ sudo dnf install nginx
```
Rocky Linux 9 - BaseOS                                                                  9.4 kB/s | 4.1 kB     00:00
Rocky Linux 9 - BaseOS                                                                  1.4 MB/s | 2.2 MB     00:01
Rocky Linux 9 - AppStream                                                               6.3 kB/s | 4.5 kB     00:00
Rocky Linux 9 - AppStream                                                               763 kB/s | 7.4 MB     00:09
Rocky Linux 9 - Extras                                                                  5.7 kB/s | 2.9 kB     00:00
Rocky Linux 9 - Extras                                                                   10 kB/s |  14 kB     00:01
Dependencies resolved.
========================================================================================================================
 Package                         Architecture         Version                             Repository               Size
========================================================================================================================
Installing:
 nginx                           x86_64               1:1.20.1-14.el9_2.1                 appstream                36 k
Installing dependencies:
 nginx-core                      x86_64               1:1.20.1-14.el9_2.1                 appstream               565 k
 nginx-filesystem                noarch               1:1.20.1-14.el9_2.1                 appstream               8.5 k
 rocky-logos-httpd               noarch               90.14-2.el9                         appstream                24 k

Transaction Summary
========================================================================================================================
Install  4 Packages

Total download size: 634 k
Installed size: 1.8 M
Is this ok [y/N]: Y
Downloading Packages:
(1/4): nginx-filesystem-1.20.1-14.el9_2.1.noarch.rpm                                     15 kB/s | 8.5 kB     00:00
(2/4): rocky-logos-httpd-90.14-2.el9.noarch.rpm                                          39 kB/s |  24 kB     00:00
(3/4): nginx-1.20.1-14.el9_2.1.x86_64.rpm                                                50 kB/s |  36 kB     00:00
(4/4): nginx-core-1.20.1-14.el9_2.1.x86_64.rpm                                          718 kB/s | 565 kB     00:00
------------------------------------------------------------------------------------------------------------------------
Total                                                                                   322 kB/s | 634 kB     00:01
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Running scriptlet: nginx-filesystem-1:1.20.1-14.el9_2.1.noarch                                                    1/4
  Installing       : nginx-filesystem-1:1.20.1-14.el9_2.1.noarch                                                    1/4
  Installing       : nginx-core-1:1.20.1-14.el9_2.1.x86_64                                                          2/4
  Installing       : rocky-logos-httpd-90.14-2.el9.noarch                                                           3/4
  Installing       : nginx-1:1.20.1-14.el9_2.1.x86_64                                                               4/4
  Running scriptlet: nginx-1:1.20.1-14.el9_2.1.x86_64                                                               4/4
  Verifying        : rocky-logos-httpd-90.14-2.el9.noarch                                                           1/4
  Verifying        : nginx-filesystem-1:1.20.1-14.el9_2.1.noarch                                                    2/4
  Verifying        : nginx-1:1.20.1-14.el9_2.1.x86_64                                                               3/4
  Verifying        : nginx-core-1:1.20.1-14.el9_2.1.x86_64                                                          4/4

Installed:
  nginx-1:1.20.1-14.el9_2.1.x86_64                              nginx-core-1:1.20.1-14.el9_2.1.x86_64
  nginx-filesystem-1:1.20.1-14.el9_2.1.noarch                   rocky-logos-httpd-90.14-2.el9.noarch

Complete!
```


🌞 Créer le site web

[lunacf@web ~]$ sudo cat var/www/site_web_nul/index.html
```
<h1>MEOW</h1>
```

🌞 Donner les bonnes permissions

```
[lunacf@web www]$ ls -al
total 0
drwxr-xr-x. 3 lunacf lunacf 26 Dec  1 01:26 .
drwxr-xr-x. 3 lunacf lunacf 17 Dec  1 01:22 ..
drwxr-xr-x. 2 nginx  nginx  24 Dec  1 01:26 site_web_nul
```

🌞 Créer un fichier de configuration NGINX pour notre site web


[lunacf@web ~]$ sudo cat /etc/nginx/conf.d/site_web_nul.conf
```
server {
  # le port sur lequel on veut écouter
  listen 80;

  # le nom de la page d'accueil si le client de la précise pas
  index index.html;

  # un nom pour notre serveur (pas vraiment utile ici, mais bonne pratique)
  server_name www.site_web_nul.b1;

  # le dossier qui contient notre site web
  root /var/www/site_web_nul;
}
```

🌞 Démarrer le serveur web !

[lunacf@web ~]$ sudo systemctl start nginx
[lunacf@web ~]$ sudo systemctl status nginx
```
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; preset: disabled)
     Active: active (running) since Fri 2023-12-01 02:15:45 CET; 24s ago
    Process: 11493 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 11494 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 11495 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 11496 (nginx)
      Tasks: 2 (limit: 4673)
     Memory: 2.0M
        CPU: 20ms
     CGroup: /system.slice/nginx.service
             ├─11496 "nginx: master process /usr/sbin/nginx"
             └─11497 "nginx: worker process"

Dec 01 02:15:45 web systemd[1]: Starting The nginx HTTP and reverse proxy server...
Dec 01 02:15:45 web nginx[11494]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Dec 01 02:15:45 web nginx[11494]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Dec 01 02:15:45 web systemd[1]: Started The nginx HTTP and reverse proxy server.
```

🌞 Ouvrir le port firewall

[lunacf@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
```
success
```

[lunacf@web ~]$ sudo firewall-cmd --reload
```
success
```

🌞 Visitez le serveur web !

[lunacf@node ~]$ curl 10.5.1.12
```
<h1>MEOW</h1>
```

🌞 Visualiser le port en écoute

[lunacf@web ~]$ sudo ss -l -n |grep -i 80
```
tcp   LISTEN 0      511                                       0.0.0.0:80               0.0.0.0:*
tcp   LISTEN 0      511                                          [::]:80                  [::]:*
```
[lunacf@web ~]$ grep -r -E "listen\s+80;" /etc/nginx/conf.d/
```
/etc/nginx/conf.d/site_web_nul.conf:  listen 80;
```

🌞 Analyse trafic

!(🦈tp5_web)[tp5_web.pcap]