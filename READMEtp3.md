# TP3 : On va router des trucs

Au menu de ce TP, on va revoir un peu ARP et IP histoire de **se mettre en jambes dans un environnement avec des VMs**.

Puis on mettra en place **un routage simple, pour permettre à deux LANs de communiquer**.

![Reboot the router](./img/reboot.jpeg)

## Sommaire

- [TP3 : On va router des trucs](#tp3--on-va-router-des-trucs)
  - [Sommaire](#sommaire)
  - [0. Prérequis](#0-prérequis)
  - [I. ARP](#i-arp)
  - [II. Routage](#ii-routage)

## 0. Prérequis

➜ Pour ce TP, on va se servir de VMs Rocky Linux. 1Go RAM c'est large large. Vous pouvez redescendre la mémoire vidéo aussi.  

➜ Vous aurez besoin de deux réseaux host-only dans VirtualBox :

- un premier réseau `10.3.1.0/24`
- le second `10.3.2.0/24`
- **vous devrez désactiver le DHCP de votre hyperviseur (VirtualBox) et définir les IPs de vos VMs de façon statique**

➜ Les firewalls de vos VMs doivent **toujours** être actifs (et donc correctement configurés sinon, ça fonctionnera po).

➜ Vous utiliserez toujours SSH (pas directement la console VirtualBox)

➜ **Si vous voyez le p'tit pote 🦈 c'est qu'il y a un fichier PCAP à produire et à mettre dans votre dépôt git de rendu (une capture Wireshark).**

> *Pour un TP plus clair, j'ai séparé chaque partie dans un doc dédié.*

## I. [ARP](./1_arp.md)

## II. [Routage](./2_routing.md)
