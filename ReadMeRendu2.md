rendu tp2

🌞 connexion est fonctionnelle entre les deux machines
```
        ping 10.33.48.31
        Statistiques Ping pour 10.33.48.31:
            Paquets : envoyés = 4, 
            reçus = 4, perdus = 0 (perte 0%)
```

🌞 Wireshark it

![🦈capture wireshark🦈](toto.pcapng)


🌞 Check the ARP table
```
    arp -a
        adresse MAC binôme : 
            48-68-4a-b3-70-0e
        adresse MAC gateway : 
            a6-f1-ba-74-f1-02
```
    
🌞 Manipuler la table ARP
```
    ip n s


```
A FAIRE 


🌞 Manipuler la table ARP

utilisez une commande pour vider votre table ARP
prouvez que ça fonctionne en l'affichant et en constatant les changements
ré-effectuez des pings, et constatez la ré-apparition des données dans la table ARP


Les échanges ARP sont effectuées automatiquement par votre machine lorsqu'elle essaie de joindre une machine sur le même LAN qu'elle. Si la MAC du destinataire n'est pas déjà dans la table ARP, alors un échange ARP sera déclenché.

🌞 Wireshark it

vous savez maintenant comment forcer un échange ARP : il sufit de vider la table ARP et tenter de contacter quelqu'un, l'échange ARP se fait automatiquement
mettez en évidence les deux trames ARP échangées lorsque vous essayez de contacter quelqu'un pour la "première" fois

déterminez, pour les deux trames, les adresses source et destination
déterminez à quoi correspond chacune de ces adresses



🦈 PCAP qui contient les DEUX trames ARP

L'échange ARP est constitué de deux trames : un ARP broadcast et un ARP reply.