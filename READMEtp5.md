# TP5 : TCP, UDP et services réseau

Dans ce TP on va explorer un peu les protocoles TCP et UDP. 

**La première partie est détente**, vous explorez TCP et UDP un peu, en vous servant de votre PC.

La seconde partie se déroule en environnement virtuel, avec des VMs. Les VMs vont nous permettre en place des services réseau, qui reposent sur TCP et UDP.  
**Le but est donc de commencer à mettre les mains de plus en plus du côté administration, et pas simple client.**

Dans cette seconde partie, vous étudierez donc :

- le protocole SSH (contrôle de machine à distance)
- le protocole DNS (résolution de noms)
  - essentiel au fonctionnement des réseaux modernes

![TCP UDP](./img/tcp_udp.jpg)

# Sommaire

- [TP5 : TCP, UDP et services réseau](#tp5--tcp-udp-et-services-réseau)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
- [I. First steps](#i-first-steps)
- [II. Setup Virtuel](#ii-setup-virtuel)
  - [1. SSH](#1-ssh)
  - [2. Routage](#2-routage)
  - [3. Serveur Web](#3-serveur-web)

# 0. Prérequis

➜ **Pour ce TP, on va se servir de VMs Rocky Linux** quand des VMs seront nécessaires. On va en faire plusieurs, n'hésitez pas à diminuer la RAM (512Mo ou 1Go devraient suffire). Vous pouvez redescendre la mémoire vidéo aussi.  

➜ **Si vous voyez un 🦈** c'est qu'il y a une capture PCAP à produire et à mettre dans votre dépôt git de rendu

➜ **L'emoji 🖥️ indique une VM à créer**. Pour chaque VM, vous déroulerez la checklist suivante :

- [x] Créer la machine (avec une carte host-only)
- [x] Définir une IP statique à la VM
- [x] Donner un hostname à la machine
- [x] Utiliser SSH pour administrer la machine
- [x] Dès que le routeur est en place, n'oubliez pas d'ajouter une route par défaut aux autres VM pour qu'elles aient internet

> Toutes les commandes pour réaliser ces opérations sont dans [le mémo Rocky](../../cours/memo/rocky_network.md). Aucune de ces étapes ne doit figurer dan le rendu, c'est juste la mise en place de votre environnement de travail.

# I. First steps

Faites-vous un petit top 5 des applications que vous utilisez sur votre PC souvent, des applications qui utilisent le réseau : un site que vous visitez souvent, un jeu en ligne, Spotify, j'sais po moi, n'importe.

🌞 **Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP**

- avec Wireshark, on va faire les chirurgiens réseau
- déterminez, pour chaque application :
  - IP et port du serveur auquel vous vous connectez
  - le port local que vous ouvrez pour vous connecter

> Rappel : quand on utilise un service en ligne, on se connecte sur un port d'un serveur. Pour ce faire, le client ouvre un port random, et se connecte à un port spécifique qu'il connaît sur le serveur. Par exemple 443 en TCP pour du trafic HTTPS. Le client doit savoir à l'avance à quelle IP et à quel port il se connecte.

🌞 **Demandez l'avis à votre OS**

- votre OS est responsable de l'ouverture des ports, et de placer un programme qui se "lie" à un port (on dit généralement qu'il se *bind* à un port)
- l'OS garde une trace en permanence de quelle programme utilise quel port
- utilisez la commande adaptée à votre OS pour repérer, dans la liste de toutes les connexions réseau établies, la connexion que vous voyez dans Wireshark, pour chacune des 5 applications
- **lancez votre terminal en admin pour avoir toutes les infos**

**Il faudra ajouter des options adaptées aux commandes pour y voir clair. Pour rappel, vous cherchez des connexions TCP ou UDP.**

```
# MacOS
$ netstat

# GNU/Linux
$ ss

# Windows
$ netstat
```

🦈🦈🦈🦈🦈 **Bah ouais, 5 captures Wireshark à l'appui évidemment.** Ce sera `tp5_service_1.pcapng` jusqu'à `tp5_service_5.pcapng`.

> Une capture pour chaque application, qui met bien en évidence le trafic en question. C'est à dire on ne doit voir QUE le trafic de l'application en question. Vous devez donc utiliser un filtre dans Wireshark pour isoler le trafic que vous souhaitez.

# II. Setup Virtuel

## 1. SSH

| Machine        | Réseau `10.5.1.0/24` |
| -------------- | -------------------- |
| `node1.tp5.b1` | `10.5.1.11`          |

🖥️ **Machine `node1.tp5.b1`**

- n'oubliez pas de dérouler la checklist (voir [les prérequis du TP](#0-prérequis))
- donnez lui l'adresse IP `10.5.1.11/24`

Connectez-vous en SSH à votre VM.

🌞 **Examinez le trafic dans Wireshark**

- **déterminez si SSH utilise TCP ou UDP**
  - pareil réfléchissez-y deux minutes, logique qu'on utilise pas UDP non ?
- **repérez le *3-Way Handshake* à l'établissement de la connexion**
  - c'est le `SYN` `SYNACK` `ACK`
- **repérez du trafic SSH**
- **repérez le FIN ACK à la fin d'une connexion**
- entre le *3-way handshake* et l'échange `FIN`, c'est juste une bouillie de caca chiffré, dans un tunnel TCP

> **SUR WINDOWS, pour cette étape uniquement**, utilisez Git Bash et PAS Powershell. Avec Powershell il sera très difficile d'observer le FIN ACK.

🌞 **Demandez aux OS**

- repérez, avec une commande adaptée (`netstat` ou `ss`), la connexion SSH **depuis votre machine**
- ET repérez la connexion SSH **depuis la VM**
  - avec une commande `ss` dans le terminal depuis `node1.tp5.b1`
  - il faudra rajouter des options pertinentes à la commandes `ss`
  - vous devez repérer clairement une ligne qui indique que c'est le programme `sshd` (le serveur SSH) qui est responsable de la connexion

🦈 **`tp5_3_way.pcapng` : une capture clean avec le 3-way handshake, un peu de trafic au milieu et une fin de connexion**

> Là encore, dans la capture, il ne doit y avoir QUE votre connexion SSH, du commencement de la session SSH, jusqu'à ce qu'elle se termine.

## 2. Routage

Ouais, un peu de répétition, ça fait jamais de mal. On va créer une machine qui sera notre routeur, et **permettra à toutes les autres machines du réseau d'avoir Internet.**

> On fera souvent ça désormais dans les TPs : toutes nos VMs et notre PC sont connectés à un switch host-only. Tout le monde a une IP dans le même réseau. Dans ce réseau, une des machines est un routeur, elle donne internet aux autres.

| Machine         | Réseau `10.5.1.0/24` |
| --------------- | -------------------- |
| `node1.tp5.b1`  | `10.5.1.11`          |
| `router.tp5.b1` | `10.5.1.254`         |

🖥️ **Machine `router.tp5.b1`**

- n'oubliez pas de dérouler la checklist (voir [les prérequis du TP](#0-prérequis))
- donnez lui l'adresse IP `10.5.1.254/24` sur sa carte host-only
- ajoutez-lui une carte NAT, qui permettra de donner Internet aux autres machines du réseau
- ajoutez une route par défaut à `node1.tp5.b1` pour qu'il ait un accès internet

🌞 **Prouvez que**

- `node1.tp5.b1` a un accès internet
- `node1.tp5.b1` peut résoudre des noms de domaine publics (comme `www.ynov.com`)

## 3. Serveur Web

Dans cette section on va monter un p'tit serveur Web sur une VM. Le serveur web hébergera un unique site web, qui sera super nul super moche parce qu'on est pas en cours de dév web :D

Le nom du serveur web qu'on va utiliser c'est NGINX. On l'installe, on le configure, on le lance, et PAF un site web hébergé en local disponible.

| Machine         | Réseau `10.5.1.0/24` |
| --------------- | -------------------- |
| `node1.tp5.b1`  | `10.5.1.11`          |
| `router.tp5.b1` | `10.5.1.254`         |
| `web.tp5.b1`    | `10.5.1.12`          |

🖥️ **Machine `web.tp5.b1`**

🌞 **Installez le paquet `nginx`**

- avec une commande `dnf install`

🌞 **Créer le site web**

- quand on héberge des sites web sur un serveur Linux, il existe un dossier qu'on utilise pour stocker les sites web, par convention
  - ce dossier c'est `/var/www/`
- créer donc un dossier `/var/www/site_web_nul/`
- créer un fichier `/var/www/site_web_nul/index.html` qui contient un code HTML simpliste de votre choix
  - un p'tit `<h1>MEOW</h1>` ça suffit hein

🌞 **Donner les bonnes permissions**

- l'utilisateur qui va lancer le serveur web, ce sera ni le vôtre, ni `root` mais un utilisateur dédié qui s'appelle `nginx`
- il faut donc lui donner les droits afin qu'il puisse lire le contenu du dossier et qu'il puisse servir le site web aux visiteurs
- pour ça, tapez la commande :

```bash
$ sudo chown -R nginx:nginx /var/www/site_web_nul
```

---

➜ On verra tout ça en détail en cours de Linux, mais j'aime pas jeter juste des commandes dans votre visage. Alors quelques points pour comprendre cette commande :

- dans Linux...
  - chaque fichier ou dossier est possédé par un utilisateur (forcément)
  - chaque fichier ou dossier est aussi possédé par un groupe d'utilisateurs (forcément)
  - l'utilisateur, et tous les membres du groupe, ont des droits sur le fichier/dossier en question (création, suppression, modification, exécution)
- `chown` pour "change owner" permet de changer le propriétaire d'un fichier ou d'un dossier
- on peut ajouter l'option `-R` à `chown` pour "recursif"
  - on va pas juste changer le propriétaire d'un dossier, mais aussi tous ses sous-dossiers et sous-fichiers
- `nginx:nginx`
  - le premier c'est l'utilisateur qui possédera le fichier (un user `nginx` a été créé quand vous avez installé le paquet)
  - le deuxième c'est le groupe qui possédera le fichier (ui, y'a un groupe `nginx` aussi qui a été créé)
- et enfin `/var/www/site_web_nul` : le dossier dont on veut changer le propriétaire
- et on fait ça avec `sudo` car seul un admin peut changer le propriétaire d'un fichier qui ne lui appartient pas (sinon y'aurait 0 sécu)

---

🌞 **Créer un fichier de configuration NGINX pour notre site web**

- à l'installation du paquet `nginx`, un dossier dédié à stocker la configuration de nos sites web a été créé
  - ce dossier c'est `/etc/nginx/`
- plus spécifiquement, on va ajouter une configuration dans le dossiere `/etc/nginx/conf.d/`
- créez le fichier `/etc/nginx/conf.d/site_web_nul.conf` avec le contenu suivant :

```nginx
server {
  # le port sur lequel on veut écouter
  listen 80;

  # le nom de la page d'accueil si le client de la précise pas
  index index.html;

  # un nom pour notre serveur (pas vraiment utile ici, mais bonne pratique)
  server_name www.site_web_nul.b1;

  # le dossier qui contient notre site web
  root /var/www/site_web_nul;
}
```

🌞 **Démarrer le serveur web !**

- avec une commande `sudo systemctl start nginx`
- si rien ne s'affiche, c'est que tout va bien
  - `systemctl status nginx` pour vérifier
- si quelque chose s'affiche c'est qu'il y a un soucis
  - lisez et interprétez le message d'erreur
  - si ça suffit pas/si vous galérez à comprendre, vous m'appelez

🌞 **Ouvrir le port firewall**

🌞 **Visitez le serveur web !**

- ouvrez un navigateur et taper `http://<IP_DE_LA_VM_WEB>`
  - vous remplacez évidemment `<IP_DE_LA_VM_WEB>` par... l'IP de la VM web :)
  - je veux pas ça dans le compte-rendu, ça se fait juste après en ligne de commande pour le compte-rendu
- il est possible de faire des requêtes HTTP depuis la ligne de commande avec la comande `curl`
  - utilisez la commande `curl` depuis `node1.tp5.b1` pour visiter le site web

🌞 **Visualiser le port en écoute**

- avec une commande `ss` dans le terminal depuis `web.tp5.b1`
  - il faudra ajouter des options à la commande `ss` sinon c'est imbuvable :)
  - vous devez repérer une ligne qui indique clairement que NGINX écoute derrière le port 80

🌞 **Analyse trafic**

- pendant que vous effectuez une connexion d'un client vers le serveur web qui tourne sur `web.tp5.b1`, lancez une capture réseau
- repérez :
  - le 3 way handshake TCP
  - du trafic HTTP
  - le contenu de la page HTML retourné (oui c'est visible direct dans Wireshark)

🦈 **`tp5_web.pcapng`** avec le 3-way handshake, la page HTML retournée, et une fin de connexion propre si vous en avez une :)
